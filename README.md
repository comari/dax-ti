# dax-ti
dax-ti is a dataset gathering the mechanical properties of 286 distinct multi-component Ti-based alloys. Each entry is associated with a high-quality experimental work containing a complete description of the microstructure, processing route, and testing setup. A helper script to load and filter desired entries is also provided (utils.py). A static version of the dataset has been recently submitted for publication. For more information, please visit [our page](https://comari.com.br).


## Usage
- **dax-ti-static.csv**: static version of the dataset; can be easily imported into your preferred data processing software. 
- **utils.py**: dependencies are matplotlib(3.4.3+), numpy(1.21.2+), and pymatgen(2022.0.16+).

A few examples on how to use the helper script on the terminal:
```python3
import utils
utils.plot_fig1()
print ('Conversion from wt -> at%')
a = utils.get_at_frac([('Ti', 19.0), ('Zr', 21.0), ('Nb', 19.0), ('Mo', 20.0), ('Ta', 21.0)])
print ('Atomic fractions: {}'.format(a))
c = utils.Composition('Ti0.6850Ta0.12Nb0.09Zr0.06O0.015')
print ('The MoEq of {} is {}'.format(c, utils.get_moeq(c)))
```

## Contributing
dax-ti accepts submissions of new entries to the dataset in its **sid** (rolling) version. If you wish to submit your work or contribute in any other way, please create an issue.

## Authors and acknowledgments
Initial version by C.A.F. Salvador, E.L. Maia, F. H. Costa, J.D. Escobar and J.P. Oliveira.

## License
dax-ti - experimental dataset for Ti-alloys exploration. Copyright (C) 2021 Comari

dax-ti is free software; you can distribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, version 3. See LICENSE.txt for the complete GPLv3 license.

## Please cite

**A compilation of experimental data on the mechanical properties and microstructural features of Ti-alloys**<br/>
[Data article, Periodic, Publisher, vol, issue, year](https://gitlab.com/comari)
